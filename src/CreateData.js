//------------------------------------------
// const createGraphic = require("./CreateGraphic.js");
import CreateGraphic from "./CreateGraphic.js";
//------------------------------------------
class CreateData{
//****************************************************
	#arrayMain = "Джон Доу Танибергенович_2604247453_840705301360".split("_");
	#gender = "male";//["male","female"];
	#email = "almasbekajmuratov@gmail.com";
	#phone = "+7 (704) 804-30-34";
	#address = "Республика Казахстан, область Туркестанская, город Туркестан, улица Н.Абдиров, дом №173";
	#debtArray = (() => {
		const strArray2 = [
			[-1, "Текущая сумма задолженности"],
			[-1, "Всего оплачено"],
			[1, "Тело кредита"],
			[-1, "Проценты"],
			[-1, "Комиссия"],
			[0, "Пеня"],
		]
		// "0,00\n" +
		const str = "15728,00\n" + //"Текущая сумма задолженности"
			"0,00\n" + //"Всего оплачено"
			"10898,00\n" + //"Тело кредита"
			"0,00\n" + //"Проценты"
			"0,00\n" + //"Комиссия"
			"4830,00"; //"Пеня"

		const strArray = str
			.replace(/,/g, ".")
			.split("\n");

		return strArray2.map((item, index) => {
			const [val1, val2] = item;
			return [strArray[index], val2, val1];
		});

		// return strArray;
	})();
	#condition = {
		logic: ["noData", "period", "sum"].find(item => item === "period"),
		// клиент может попросить разбить долг на месяцы,
		// или исходя из возможности оплачивать кредит
		// определенной суммой в месяц
		period: 2,
		debt:(()=>{
			const total = this.#debtArray.reduce((acc,item)=>{
				const [sum,description,status] = item;
				if(status === 1)
					acc+= (+sum);
				return acc;
			},0);
			// console.log(">>>>>>>>>>>>>>>",total)
			return total;
		})(),
		promiseP: 0,
		startD: "15.09.2023"
	}
	#conditionArray = [];
//###################################################################
//###################################################################
//###################################################################
//###################################################################
	constructor(){
		const createGraphic = new CreateGraphic();
		this.#conditionArray = createGraphic.getArray(this.#condition);
		// console.log(createGraphic.getArray())
		// console.log(this.#debtArray);
		// new createGraphic(122_397,11000,"10.9.2023");
	}

//###################################################################
//###################################################################
//###################################################################
//###################################################################
	getData(){
		return {
			arrayMain: this.#arrayMain,
			gender:this.#gender,
			email: this.#email,
			phone: this.#phone,
			address: this.#address,
			billData: this.#debtArray,
			conditionArray: this.#conditionArray
		}
	}
}
//###################################################################
//###################################################################
//###################################################################
//###################################################################
const createData = new CreateData();
console.log(createData.getData())

//------------------------------------------
// module.exports = CreateGraphic;
export default CreateData;
//import CreateData from "./CreateData.js";
//------------------------------------------
