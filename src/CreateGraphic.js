//------------------------------------------
// const NumToCursive = require("./NumToCursive");
import NumToCursive from "./NumToCursive.js";

//------------------------------------------
class CreateGraphic extends NumToCursive{
	constructor(){
		super();
	}

//###################################################################
	getArray({
		         logic = "noData",
		         period = 0,
		         debt = 0,
		         promiseP = 0,
		         startD = "10.9.2023"
	         }){
		//^^^^^^^^^^^^^^^^^^
		const startDay = startD.split(".");
		const promisePay = promiseP;
		let date = new Date(
			startDay[2].toString(),
			startDay[1].toString(),
			startDay[0].toString()
		);
		this.#getDateFuture(date, 2, "minus");
		//^^^^^^^^^^^^^^^^^^
		const arrayStr = [];
		if(logic === "noData"){
			arrayStr.push("нет данных для отображения!");
		}else if(logic === "sum"){
			arrayStr.push(`Сумма задолженности ${this.splitNumIntoDigits(debt)} (${this.setNumber(debt)}) тенге:`);
			for(let i = 0; true; i++){
				arrayStr.push(`${this.splitNumIntoDigits(promisePay)} (${this.setNumber(promisePay)}) тенге до ${this.#getDateFuture(date, 1, "Plus")} года;`);
				debt -= promisePay;
				if(debt < promisePay){
					arrayStr.push(`${this.splitNumIntoDigits(debt)} (${this.setNumber(debt)}) тенге до ${this.#getDateFuture(date, 1, "Plus")} года;`);
					break;
				}
			}
		}else{
			const payInMonth = Math.round(debt / period);
			arrayStr.push(`Сумма задолженности ${this.splitNumIntoDigits(debt)} (${this.setNumber(debt)}) тенге:`);
			// arrayStr.push(payInMonth)
			for(let i = 0; i < period; i++){
				arrayStr.push(`${this.splitNumIntoDigits(payInMonth)} (${this.setNumber(payInMonth)}) тенге до ${this.#getDateFuture(date, 1, "Plus")} года;`);
			}
		}
		//-------------------------------------
		return arrayStr;
	}

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	#getDateFuture(date, month, minusOrPlus){
		const months = ["Январь", "Февраль",
			"Март", "Апрель", "Май",
			"Июнь", "Июль", "Август",
			"Сентябрь", "Октябрь", "Ноябрь",
			"Декабрь"];

		if(minusOrPlus === "minus"){
			date.setMonth(date.getMonth() - month);
		}else{
			date.setMonth(date.getMonth() + month);
		}
		return `${date.getDate()} ${months[date.getMonth()]} ${date.getFullYear()}`
	}

//-----------------------------------------------------------------------------------------
}

//############################################################################################
//############################################################################################
//############################################################################################
//############################################################################################
// const createGraphic = new CreateGraphic(122_397, 11000, "10.9.2023");
// console.log(createGraphic.getArray())

//------------------------------------------
// module.exports = CreateGraphic;
export default CreateGraphic;
//import CreateGraphic from "./CreateGraphic.js";
//------------------------------------------