//https://docx.js.org/#/usage/paragraph
import * as fs from "fs";
import CreateData from "./CreateData.js";
import CreateGraphic from "./CreateGraphic.js";
import NumToCursive from "./NumToCursive.js";

import {
	Table, TableCell, TableRow, WidthType,
	TabStopType,
	TabStopPosition,
	Tab,
	AlignmentType,
	convertInchesToTwip,
	Document,
	HeadingLevel,
	LevelFormat,
	Packer,
	Paragraph,
	TextRun,
	UnderlineType,
} from "docx";

class DocxClass extends NumToCursive{

	// #createGraphicObj = null;
	// #numToCursive = null;
	#doc = null;

	constructor({
		            arrayMain = ['ФИО', 'договор', 'ИИН'],
		            gender = "female",
		            email = 'noEmail',
		            phone = 'noPhone',
		            address = 'noAddress',
		            billData = [],
		            conditionArray = []
	            }){
		super();
		// this.#createGraphicObj = new CreateGraphic().splitNumIntoDigits;
		// console.log(this.#createGraphicObj(22222222))//22 222 222

		console.log(arrayMain)
		console.log(email)
		console.log(phone)
		console.log(address)
		console.log(billData)
		console.log(conditionArray)

		this.#doc = new Document({
			creator: "Clippy",
			title: "Sample Document",
			description: "A brief example of using docx",
			styles: {
				default: {
// -----------------------------------
					heading1: {
						run: {
							size: 28,
							bold: true,
							italics: false,
							// color: "#FF0000",
						},
						paragraph: {
							alignment: AlignmentType.CENTER,
							spacing: {
								before: 240,
								after: 240,
							},
						},
					},
// -----------------------------------
					heading2: {
						run: {
							size: 26,
							bold: true,
							underline: {
								type: UnderlineType.DOUBLE,
								color: "FF0000",
							},
						},
						paragraph: {
							spacing: {
								before: 240,
								after: 120,
							},
						},
					},
// -----------------------------------
				},
				paragraphStyles: [
					//id: "aside",
					this.#paragraphStylesAside(),
					this.#paragraphStylesBody(),
				],
			},
			sections: [
				{
					children: [
						new Paragraph(""),
						new Paragraph(""),
						new Paragraph(""),
						new Paragraph(""),
						...this.#createAsideData({
							FIO: arrayMain[0],
							IIN: arrayMain[2],
							email: email,
							phone: phone,
							address: address
						}),
//----------------------------------------------
						// this.#createCenterData(),
						new Paragraph({
							text: "Ответ на заявление",
							heading: HeadingLevel.HEADING_1,
						}),
//----------------------------------------------
						...this.#createBodyData_1(arrayMain[0],gender),
						new Paragraph(""),
						this.#createBodyTable(billData),
						new Paragraph(""),
						...this.#createBodyData_2({billData, arrayMain}),
						...this.#createBodyData_3(),
						new Paragraph(""),
						new Paragraph({
							children: [
								new TextRun({
									children: [
										new Tab(),
										new TextRun({
											text: "По всем возникающим вопросам должник может обращаться также и в Агентство: office@ccg.com.kz, azamat.dzhunusov@ccg.com.kz, а также позвонив по телефонам: +7(727)330-97-51 вн.32147;  +7(707)916-50-76.",
											bold: false,
										})
									],
								}),
							],
							style: "body",
						}),
						new Paragraph(""),
						...this.#createBodyFooter(),
//###################################################################
// NEW PAGE
//###################################################################
						new Paragraph({
							text: "График погашения задолженности",
							heading: HeadingLevel.HEADING_1,
							pageBreakBefore: true,
						}),
//----------------------------------------------
						...this.#createCreateInformation({arrayMain, conditionArray}),


					],
				},
			],
		});
	}

//###################################################################
//###################################################################
//###################################################################
//###################################################################
	getDoc(){
		return this.#doc;
	}

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside
// Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside
// Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside
// Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside
	#createAsideData({
		                 FIO = 'ФИО',
		                 IIN = 'ИИН',
		                 email = 'noEmail',
		                 phone = 'noPhone',
		                 address = 'noAddress',
	                 }){
		const innerArray = [];
		const arrayKeyValue = [
			["Кому: ", FIO],
			["ИИН: ", IIN],
			["Эл.почта: ", email],
			["Тел: ", phone],
			["Адрес: ", address],
		];

		for(const element of arrayKeyValue){
			const [key, value] = element;
			innerArray.push(
				new Paragraph({
					children: [
						new TextRun({
							text: key,
							bold: true,
						}),
						new TextRun(value),
					],
					style: "aside",
				})
			);
		}
		innerArray.push(
			new Paragraph({
				children: [
					new TextRun({
						text: "От:",
						bold: true,
						break: 1,// отступ от Адрес: Almaty
					}),
					new TextRun({
						text: " ТОО «Коллекторское агентство «Кредит Коллекшн Груп»",
					}),
				],
				style: "aside",
			})
		)
		return innerArray;
	};

	#paragraphStylesAside(){
		return {
			id: "aside",
			name: "Aside",
			basedOn: "Normal",
			next: "Normal",
			run: {
				size: 24,
				// italics: true,
				// color: "999999",
			},
			paragraph: {
				indent: {
					left: convertInchesToTwip(3.5),
				},
				// spacing: {
				// 	line: 276,
				// },
			},
		}
	};

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// Body Body Body Body Body Body Body Body Body Body Body Body Body
// Body Body Body Body Body Body Body Body Body Body Body Body Body
// Body Body Body Body Body Body Body Body Body Body Body Body Body
// Body Body Body Body Body Body Body Body Body Body Body Body Body
	#createBodyData_1(FIO,gender){
		return [
			new Paragraph({
				children: [
					new TextRun({
						children: [new Tab(),`${gender==="male" ? "Уважаемый" : "Уважаемая"} ${FIO}, мы внимательно рассмотрели ваше обращение и сообщаем следующее:`],
					}),
				],
				style: "body",
			}),
			new Paragraph({
				children: [
					new TextRun({
						children: [new Tab(), `На сегодняшний день Ваша задолженность перед кредитором выглядит следующим образом:`],
					}),
				],
				style: "body",
			}),
		]
	};

	#createBodyData_2({
		                  billData = [
			                  ['0.00', 'Текущая сумма задолженности', -1],
			                  ['0.00', 'Всего оплачено', -1],
			                  ['0.00', 'Тело кредита', 1],
			                  ['0.00', 'Проценты', 1],
			                  ['0.00', 'Комиссия', 1],
			                  ['0.00', 'Пеня', 1]
		                  ],
		                  arrayMain = []
	                  }){
// 	billData: [
// 		[ '150513.00', 'Текущая сумма задолженности', -1 ],
// 		[ '0.00', 'Всего оплачено', -1 ],
// 		[ '99999.00', 'Тело кредита', 1 ],
// 		[ '22398.00', 'Проценты', 0 ],
// 		[ '23220.00', 'Комиссия', 0 ],
// 		[ '4896.00', 'Пеня', 0 ]
// 	],
		const innerBillWritten = billData.reduce((concatStr, item) => {
			const [sum, description, status] = item;
			if(!status){
				concatStr += (description.toLowerCase() + ", ")
			}
			return concatStr;
		}, "")?.trim().replace(/,$/, ".");

		const innerPayBill = billData.reduce((concatStr, item) => {
			const [sum, description, status] = item;
			if(status === 1){
				concatStr += (description.toLowerCase() + ", ")
			}
			return concatStr;
		}, "")?.trim().replace(/,$/, "")

		const innerTotal = billData.reduce((accSum, item) => {
			const [sum, description, status] = item;
			if(status === 1){
				accSum += +sum;
			}
			return accSum;
		}, 0);

		//arrayMain = ['ФИО', 'договор', 'ИИН'],
		return [
			new Paragraph({
				children: [
					new TextRun({
						children: [new Tab(), `По Вашей задолженности (номер договора ${arrayMain[1]}), согласно Вашей просьбе, будут списаны: ${innerBillWritten}`],
					}),
				],
				style: "body",
			}),
			new Paragraph({
				children: [
					new TextRun({
						children: [new Tab(), `По оставшейся задолженности: (${innerPayBill}) в общей сумме ${this.splitNumIntoDigits(innerTotal)} (${this.setNumber(innerTotal)}) тенге, будет составлен график погашения.`],
					}),
				],
				style: "body",
			}),
		]
	};

	#createBodyData_3(){
		return [
			new Paragraph(""),
			new Paragraph({
				children: [
					new TextRun({
						children: [
							new Tab(),
							new TextRun({
								text: "приложение:",
								bold: true,
							})
						],
					}),
				],
				style: "body",
			}),
			new Paragraph({
				children: [
					new TextRun({
						children: [new Tab(), "График погашения."],
					}),
				],
				style: "body",
			}),
		]
	};

	#paragraphStylesBody(){
		return {
			id: "body",
			name: "Body",
			basedOn: "Normal",
			next: "Normal",
			run: {
				size: 24,
			},
		}
	};

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// Table Table Table Table Table Table Table Table Table Table Table
// Table Table Table Table Table Table Table Table Table Table Table
// Table Table Table Table Table Table Table Table Table Table Table
// Table Table Table Table Table Table Table Table Table Table Table
	#createBodyTable(billData){
// 	billData: [
// 		[ '150513.00', 'Текущая сумма задолженности', -1 ],
// 		[ '0.00', 'Всего оплачено', -1 ],
// 		[ '99999.00', 'Тело кредита', 1 ],
// 		[ '22398.00', 'Проценты', 0 ],
// 		[ '23220.00', 'Комиссия', 0 ],
// 		[ '4896.00', 'Пеня на сегодня', 0 ]
// 	],
		const sizeCell2 = 3500;
		const sizeCell1 = 2000;
		const sizeCell0 = 1500;

		return new Table({

			alignment: AlignmentType.CENTER,
			columnWidths: [sizeCell1, sizeCell2, sizeCell0],
			rows: billData.map(item => {
				const [sum, description, status] = item;
				return new TableRow({
					children: [
						//-----------------------------
						new TableCell({
							width: {
								size: sizeCell2,
								type: WidthType.DXA,
							},
							children: [new Paragraph({
								children: [
									new TextRun({
										text: `${description}`,
										bold: false,
									}),
								],
								style: "body",
							})],
						}),
						//-----------------------------
						new TableCell({
							width: {
								size: sizeCell1,
								type: WidthType.DXA,
							},
							children: [new Paragraph({
								alignment: AlignmentType.RIGHT,
								children: [
									new TextRun({
										text: `${sum} тенге`,
										bold: false,
									}),
								],
								style: "body",
							})],
						}),
						//-----------------------------
						new TableCell({
							width: {
								size: sizeCell0,
								type: WidthType.DXA,
							},
							children: [new Paragraph({
								alignment: AlignmentType.CENTER,
								children: [
									new TextRun({
										text: `${status ? "" : "списано"}`,
										bold: false,
									}),
								],
								style: "body",
							})],
						}),
						//-----------------------------
					],
				})
			})
		});
	}

// Footer Footer Footer Footer Footer Footer Footer Footer Footer Footer
// Footer Footer Footer Footer Footer Footer Footer Footer Footer Footer
// Footer Footer Footer Footer Footer Footer Footer Footer Footer Footer
// Footer Footer Footer Footer Footer Footer Footer Footer Footer Footer
	#createBodyFooter(){
//    С уважением,
//    Генеральный директор
//    ТОО «Коллекторское агентство
//    «Кредит Коллекшн Груп»							__________Красов А.С.
		return [
			new Paragraph(""),
			new Paragraph({
				children: [
					new TextRun({
						text: "С уважением,",
						bold: true,
					}),
				],
			}),
			new Paragraph({
				children: [
					new TextRun({
						text: "Генеральный директор",
						bold: true,
					}),
				],
			}),
			new Paragraph({
				children: [
					new TextRun({
						text: "ТОО «Коллекторское агентство",
						bold: true,
					}),
				],
			}),
			new Paragraph({
				tabStops: [
					{
						type: TabStopType.RIGHT,
						position: TabStopPosition.MAX,
					},
				],
				children: [
					new TextRun({
						text: "«Кредит Коллекшн Груп»",
						bold: true,
					}),
					new TextRun({
						children: [new Tab(), "_______________Красов А.С."],
						bold: true,
					}),
				],
			}),
		]
	};

// Information Information Information Information Information Information
// Information Information Information Information Information Information
// Information Information Information Information Information Information
// Information Information Information Information Information Information
	#createCreateInformation({
		                         arrayMain = ['ФИО', 'договор', 'ИИН'],
		                         conditionArray = []
	                         }){
		/*
		Номер родительского контракта: 2602415194
		Поставщик родительского контракта: АО "Хоум  Кредит Казахстан"
		Статус родительского контракта: Уступка/переуступка права требования по кредиту другому лицу
		Должник: Кропотова Наиля Сайрановна ИИН 680825402216*/
		const innerArray = [];
		const [FIO, contract, IIN] = arrayMain;
		innerArray.push(new Paragraph({
			children: [
				new TextRun({
					text: "Номер родительского контракта: ",
					bold: true,
				}),
				new TextRun({
					text: contract,
				}),
			],
			style: "body",
		}));
		innerArray.push(new Paragraph({
			children: [
				new TextRun({
					text: "Поставщик родительского контракта: ",
					bold: true,
				}),
				new TextRun({
					text: "«АО Хоум Кредит Казахстан»",
				}),
			],
			style: "body",
		}));
		innerArray.push(new Paragraph({
			children: [
				new TextRun({
					text: "Статус родительского контракта: ",
					bold: true,
				}),
				new TextRun({
					text: "Уступка/переуступка права требования по кредиту другому лицу",
				}),
			],
			style: "body",
		}));
		innerArray.push(new Paragraph({
			children: [
				new TextRun({
					text: "Должник: ",
					bold: true,
				}),
				new TextRun({
					text: `${FIO} ИИН ${IIN}`,
				}),
			],
			style: "body",
		}));
		innerArray.push(new Paragraph(""));

		// innerArray.push(new Paragraph(conditionArray[0]));
		innerArray.push(
			new Paragraph({
				children: [
					new TextRun(conditionArray[0]),
				],
				style: "body",
			})
		)

		conditionArray.forEach((item, index) => {
			if(index)
				innerArray.push(
					new Paragraph({
						children: [
							new TextRun({
								text: `${(index)}.`,
								bold: true,
							}),
							new TextRun({
								children: [new Tab(), item],
							}),
						],
						style: "body",
					})
				)
		});
		innerArray.push(new Paragraph(""));
		innerArray.push(new Paragraph({
			style: "body",
			children: [new TextRun({text: "ТОО «Коллекторское агентство «Кредит Коллекшн Груп»"})]
		}));
		innerArray.push(new Paragraph({style: "body", children: [new TextRun({text: "БИН: 090640006492"})]}));
		innerArray.push(new Paragraph({style: "body", children: [new TextRun({text: "КБЕ: 17"})]}));
		innerArray.push(new Paragraph({
			style: "body",
			children: [new TextRun({text: "Номер счета: KZ676010131000140360"})]
		}));
		innerArray.push(new Paragraph({style: "body", children: [new TextRun({text: "Народный Банк Казахстана АО"})]}));
		innerArray.push(new Paragraph({style: "body", children: [new TextRun({text: "БИК: HSBKKZKX"})]}));
		innerArray.push(new Paragraph({style: "body", children: [new TextRun({text: "КНП:4"})]}));
		innerArray.push(new Paragraph({style: "body", children: [new TextRun({text: `Номер договора: ${(contract)}`})]}));
		innerArray.push(new Paragraph({style: "body", children: [new TextRun({text: "Тел контактный: 87087009985"})]}));
		innerArray.push(new Paragraph(""));
		innerArray.push(new Paragraph(""));
		innerArray.push(new Paragraph(""));
		innerArray.push(new Paragraph(""));

		innerArray.push(
			new Paragraph({
				children: [
					new TextRun({
						text: "Генеральный директор",
						bold: true,
					}),
				],
			}));

		innerArray.push(
			new Paragraph({
				children: [
					new TextRun({
						text: "ТОО «Коллекторское агентство",
						bold: true,
					}),
				],
			}));

		innerArray.push(
			new Paragraph({
				tabStops: [
					{
						type: TabStopType.RIGHT,
						position: TabStopPosition.MAX,
					},
				],
				children: [
					new TextRun({
						text: "«Кредит Коллекшн Груп»",
						bold: true,
					}),
					new TextRun({
						children: [new Tab(), "_______________Красов А.С."],
						bold: true,
					}),
				],
			}))
		innerArray.push(new Paragraph(""));
		innerArray.push(new Paragraph(""));
		innerArray.push(new Paragraph(""));
		innerArray.push(new Paragraph(""));
		innerArray.push(
			new Paragraph({
				tabStops: [
					{
						type: TabStopType.RIGHT,
						position: TabStopPosition.MAX,
					},
				],
				children: [
					new TextRun({
						text: "Должник",
						bold: true,
					}),
					new TextRun({
						children: [new Tab(), `_______________${(() => {
							const fio = FIO.split(" ");
							if(fio.length === 3)
								return `${fio[0]} ${fio[1][0]}.${fio[2][0]}.`;
							else
								return `${fio[0]} ${fio[1][0]}.`;
						})()}`],
						bold: true,
					}),
				],
			}))

		return innerArray;
	}
}

//###################################################################
//###################################################################
//###################################################################
//###################################################################
// const obj = {
// 	arrayMain: [ 'Сакенова Жанна Толегеновна', '2600681011', '690210450501' ],
// 	email: 'aza@mail.com',
// 	phone: '877771025594',
// 	address: 'Almaty',
// 	billData: [
// 		[ '150513.00', 'Текущая сумма задолженности', -1 ],
// 		[ '0.00', 'Всего оплачено', -1 ],
// 		[ '99999.00', 'Тело кредита', 1 ],
// 		[ '22398.00', 'Проценты', 0 ],
// 		[ '23220.00', 'Комиссия', 0 ],
// 		[ '4896.00', 'Пеня на сегодня', 0 ]
// 	],
// 	conditionArray: [
// 		'Сумма задолженности 122 397 (сто двадцать две тысячи триста девяносто семь) тенге:',
// 		'11 000 (одиннадцать тысяч) тенге до 10 Октябрь 2023 года;',
// 		'11 000 (одиннадцать тысяч) тенге до 10 Ноябрь 2023 года;',
// 		'11 000 (одиннадцать тысяч) тенге до 10 Декабрь 2023 года;',
// 		'11 000 (одиннадцать тысяч) тенге до 10 Январь 2024 года;',
// 		'11 000 (одиннадцать тысяч) тенге до 10 Февраль 2024 года;',
// 		'11 000 (одиннадцать тысяч) тенге до 10 Март 2024 года;',
// 		'11 000 (одиннадцать тысяч) тенге до 10 Апрель 2024 года;',
// 		'11 000 (одиннадцать тысяч) тенге до 10 Май 2024 года;',
// 		'11 000 (одиннадцать тысяч) тенге до 10 Июнь 2024 года;',
// 		'11 000 (одиннадцать тысяч) тенге до 10 Июль 2024 года;',
// 		'11 000 (одиннадцать тысяч) тенге до 10 Август 2024 года;',
// 		'1 397 (одна тысяча триста девяносто семь) тенге до 10 Сентябрь 2024 года;'
// 	]
// }
const createData = new CreateData();
// console.log(createData.getData())
const doc = new DocxClass(createData.getData()).getDoc();

Packer.toBuffer(doc).then((buffer) => {
	console.log(buffer)
	fs.writeFileSync("../myDocs/222-declaritive-styles.docx", buffer);
});


