class NumToCursive{
	// number = "";
	#_hundreds = ["сто","двести","триста","четыреста","пятьсот","шестьсот","семьсот","восемьсот","девятьсот"];
	#_tens = ["десять","двадцать","тридцать","сорок","пятьдесят","шестьдесят","семьдесят","восемьдесят","девяносто"];
	#_units = ["три","четыре","пять","шесть","семь","восемь","девять"];
	#_tensAndUnits = ["одиннадцать","двенадцать","тринадцать","четырнадцать","пятнадцать","шестнадцать","семнадцать","восемнадцать","девятнадцать"];

	#_millionClass = this.#_createObj(["ноль","один","два"],"millionClass");
	#_thousandClass = this.#_createObj(["ноль","одна","две"],"thousandClass");
	#_unitsClass = this.#_createObj(["ноль","один","два"],"unitsClass");


	setNumber(value){
		const innerStringArray = value
			.toString()
			.replace(/\B(?=(\d{3})+(?!\d))/g,' ')
			.split(" ");

		const [m1,m2,m3] = this.#_returnNumberArray(innerStringArray.at(-3));
		const [t1,t2,t3] = this.#_returnNumberArray(innerStringArray.at(-2));
		const [u1,u2,u3] = this.#_returnNumberArray(innerStringArray.at(-1));
		// console.log(`${m1} ${m2} ${m3}`);
		// console.log(`${t1} ${t2} ${t3}`);
		// console.log(`${u1} ${u2} ${u3}`);

		const millionClass = createRankClass([m1,m2,m3],this.#_millionClass);
		const thousandClass = createRankClass([t1,t2,t3],this.#_thousandClass);
		const unitsClass = createRankClass([u1,u2,u3],this.#_unitsClass);

		// console.log(millionClass)
		// console.log(thousandClass)
		// console.log(unitsClass)
		// console.log(changeStringRank(millionClass));
		// console.log(changeStringRank(thousandClass));
		// console.log(unitsClass)

		let combineRank = "";
		combineRank += !!millionClass ? changeStringRank(millionClass)+" " : "";
		combineRank += !!thousandClass ? changeStringRank(thousandClass)+" " : "";
		combineRank += unitsClass
		// console.log(combineRank.trim() + " тенге")
		return combineRank.trim();
//-----------------------------------------
		function changeStringRank(rankClass){
			const regex = new RegExp(/(тысяч[иа]?)|(миллио[анов]+)/g);
			// const regex = new RegExp(/миллиона/g);
			const newStr = rankClass
				.replace(regex,"")
				.replace(/\s+/g, ' ');

			const appendixArr = rankClass.match(regex,"")

			// console.log(">>>> ",newStr )
			// console.log(">>>> ",appendixArr.at(-1))
			return newStr + appendixArr.at(-1) ?? ""
		}//changeStringRank
//-----------------------------------------
		function createRankClass(arr,rankClass){
			let innerStr = "";
			for(const innerObj of rankClass.hundreds){
				const [key,value] = Object.entries(innerObj)[0];
				if(key === arr[0])
					innerStr += value.includes("ноль") ? "" : value+" ";
			}
			if(/\d1[123456789]/.test(arr.join(""))){
				for(const innerObj of rankClass.tensAndUnits){
					const [key,value] = Object.entries(innerObj)[0];
					if(key === arr[1]+arr[2])
						innerStr += value.includes("ноль") ? "" : value+" ";
				}
			}else{
				for(const innerObj of rankClass.tens){
					const [key,value] = Object.entries(innerObj)[0];
					if(key === arr[1])
						innerStr += value.includes("ноль") ? "" : value+" ";
				}
				for(const innerObj of rankClass.units){
					const [key,value] = Object.entries(innerObj)[0];
					if(key === arr[2])
						innerStr += value.includes("ноль") ? "" : value+" ";
				}
			}
			// console.log(innerStr)
			return innerStr;
		}//createRankClass
//-----------------------------------------
	}//setNumber
// **************************************************************************************
// **************************************************************************************
	getNumber(){
		// console.log(this.#_millionClass.hundreds)
		// console.log(this.#_millionClass.tens)
		// console.log(this.#_millionClass.units)
		// console.log(this.#_millionClass.tensAndUnits)

		// console.log(this.#_thousandClass.hundreds)
		// console.log(this.#_thousandClass.tens)
		// console.log(this.#_thousandClass.units)
		// console.log(this.#_thousandClass.tensAndUnits)

		// console.log(this.#_unitsClass.hundreds)
		// console.log(this.#_unitsClass.tens)
		// console.log(this.#_unitsClass.units)
		// console.log(this.#_unitsClass.tensAndUnits)
	}

//**************************************************************************************
//**************************************************************************************
	#_createObj(array,setRankClass){
		return {
//hundreds ________________________________________________________________________
			hundreds:["ноль",...this.#_hundreds].map((item,index) =>
				({[index]:`${item}${declinationFun(item,setRankClass)}`})),
//tens ____________________________________________________________________________
			tens:["ноль",...this.#_tens].map((item,index) =>
				({[index]:`${item}${declinationFun(item,setRankClass)}`})),
//units ___________________________________________________________________________
			units:[
				...array,
				...this.#_units
			].map((item,index) =>
				({[index]:`${item}${declinationFun(item,setRankClass)}`})),
//tensAndUnits ____________________________________________________________________
			tensAndUnits:this.#_tensAndUnits.map((item,index) =>
				({[index + 11]:`${item}${declinationFun(item,setRankClass)}`})),
		}

		function declinationFun(item,discharge){
			switch(discharge){
				case "millionClass":
					switch(item){
						case "один":
							return " миллион"
						case "два" :
						case "три" :
						case "четыре" :
							return " миллиона"
						case "ноль":
						case "пять":
						case "шесть" :
						case "семь" :
						case "восемь" :
						case "девять":
						case "одиннадцать" :
						case "двенадцать" :
						case "тринадцать" :
						case "четырнадцать" :
						case "пятнадцать" :
						case "шестнадцать" :
						case "семнадцать" :
						case "восемнадцать" :
						case "девятнадцать" :
						case "десять" :
						case "двадцать" :
						case "тридцать" :
						case "сорок" :
						case "пятьдесят" :
						case "шестьдесят" :
						case "семьдесят" :
						case "восемьдесят" :
						case "девяносто" :
						case "сто" :
						case "двести" :
						case "триста" :
						case "четыреста" :
						case "пятьсот" :
						case "шестьсот" :
						case "семьсот" :
						case "восемьсот" :
						case "девятьсот" :
							return " миллионов"
					}
					break


				case "thousandClass":
					switch(item){
						case "одна":
							return " тысяча"
						case "две" :
						case "три" :
						case "четыре" :
							return " тысячи"
						case "ноль":
						case "пять":
						case "шесть" :
						case "семь" :
						case "восемь" :
						case "девять":
						case "одиннадцать" :
						case "двенадцать" :
						case "тринадцать" :
						case "четырнадцать" :
						case "пятнадцать" :
						case "шестнадцать" :
						case "семнадцать" :
						case "восемнадцать" :
						case "девятнадцать" :
						case "десять" :
						case "двадцать" :
						case "тридцать" :
						case "сорок" :
						case "пятьдесят" :
						case "шестьдесят" :
						case "семьдесят" :
						case "восемьдесят" :
						case "девяносто" :
						case "сто" :
						case "двести" :
						case "триста" :
						case "четыреста" :
						case "пятьсот" :
						case "шестьсот" :
						case "семьсот" :
						case "восемьсот" :
						case "девятьсот" :
							return " тысяч"
					}
					break

				case "unitsClass":
					return ""

			}
		}
	}

//**************************************************************************************
//**************************************************************************************
	#_returnNumberArray(arrayStr){
		// console.log(arrayStr[0]);
		// console.log(arrayStr[1]);
		// console.log(arrayStr[2]);
		// console.log(arrayStr.length === 3)
		// console.log(arrayStr.length)
		if(arrayStr)
			switch(arrayStr.length){
				case 3:
					return [arrayStr[0],arrayStr[1],arrayStr[2]];
				case 2:
					return ['0',arrayStr[0],arrayStr[1]];
				case 1:
					return ['0','0',arrayStr[0]];
			}
		else
			return ['0','0','0'];

		// switch(innerStringArray){
		// 	case undefined: return [0,0,0];
		// 	case innerStringArray.length === 3: return innerStringArray;
		// 	case innerStringArray.length === 2: return [0,innerStringArray.at(-2),innerStringArray.at(-1)];
		// 	case innerStringArray.length === 1: return [0,0,innerStringArray.at(-1)];
		// }
	}
//**************************************************************************************
//**************************************************************************************
	splitNumIntoDigits(promisePay = 0){
		return promisePay.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
	}
//**************************************************************************************
//**************************************************************************************
}

// const numToCursive = new NumToCursive();
// console.log(numToCursive.setNumber(498714));
// numToCursive.setNumber(
// 	19_124_234
// )
//------------------------------------------
// module.exports = NumToCursive;
export default NumToCursive;
//------------------------------------------